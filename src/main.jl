# Image
const ASPECT_RATIO = 16.0/9.0
const IMAGE_WIDTH = 400
const IMAGE_HEIGHT = cf(IMAGE_WIDTH/ASPECT_RATIO)
const SAMPLES_PER_PIXEL = 100
const MAX_DEPTH = 50

# Camera
const CAM = Camera()

# Output
const HEADER = "P3\n$IMAGE_WIDTH $IMAGE_HEIGHT\n255\n"

function ray_color(r::Ray, world::HittableList, depth::Int)
    rec = HitRecord()

    depth <= 0 && (return [0.0, 0.0, 0.0])

    if hit!(world, r, 0.001, Inf, rec)
        target = rec.p + rec.normal + random_in_hemisphere(rec.normal) # random_unit_vector()
        return 0.5 * ray_color(Ray(rec.p, target - rec.p), world, depth-1)
    end

    # background
    unit_direction = unit_vector(r.dir)
    t = 0.5*(unit_direction[2] + 1.0)
    (1.0 - t)*[1.0, 1.0, 1.0] + t*[0.5, 0.7, 1.0] # lerp
end

function main()
    world = HittableList()
    add!(world, Sphere([0.0, 0.0, -1.0], 0.5))
    add!(world, Sphere([0.0, -100.5, -1.0], 100.0))

    open("rendering.ppm", "w") do io
        write(io, HEADER)

        for j=IMAGE_HEIGHT-1:-1:0, i=0:IMAGE_WIDTH-1
            i<1 && write(stderr, "\nScanlines remaining: $j")

            pixel_color = [0.0, 0.0, 0.0]
            for s=0:SAMPLES_PER_PIXEL-1
                u = (i + random_double())/(IMAGE_WIDTH-1)
                v = (j + random_double())/(IMAGE_HEIGHT-1)
                r = get_ray(CAM, u, v)
                pixel_color += ray_color(r, world, MAX_DEPTH)
            end

            write_color(io, pixel_color, SAMPLES_PER_PIXEL)
        end
    end
    write(stderr, "\nDone.\n")
end
