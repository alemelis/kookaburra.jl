function write_color(io::IO, c::Color, samples_per_pixel::Int)
    # scale by numer of samples and gamma-correct for gamma=2.0
    scale = 1.0/samples_per_pixel
    r, g, b = clamp.(.√(c*scale), 0.0, 0.999)
    write(io, join(cf.(256 .* [r, g, b]), " ")*"\n")
end

