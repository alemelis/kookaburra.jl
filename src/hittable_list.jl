Base.@kwdef mutable struct HittableList
    objects::Vector{Hittable} = []
end

clear!(hl::HittableList) = empty!(hl.objects)
add!(hl::HittableList, hittable::Hittable) = push!(hl.objects, hittable)

function hit!(hl::HittableList, r::Ray, t_min::Float64, t_max::Float64, rec::HitRecord)
    temp_rec = HitRecord()
    hit_anything = false
    closest_so_far = t_max

    for object in hl.objects
        if hit!(object, r, t_min, closest_so_far, temp_rec)
            hit_anything = true
            closest_so_far = temp_rec.t
            copy!(rec, temp_rec)
       end
    end

    hit_anything
end

