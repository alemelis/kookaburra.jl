module kookaburra

    import Base    
    using LinearAlgebra: ⋅, ×

    include("utils.jl")
    include("vec3.jl")
    include("ray.jl")
    include("hittable.jl")
    include("hittable_list.jl")
    include("sphere.jl")
    include("camera.jl")
    include("color.jl")
    include("main.jl")

end
