struct Ray
    orig::Point3
    dir::Vec3
end

# origin(r::Ray) = r.orig
# direction(r::Ray) = r.dir

at(r::Ray, t::Float64) = muladd(r.dir, t, r.orig)
