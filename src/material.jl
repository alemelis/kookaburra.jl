abstract type Material end

scatter(m::Material, r_in::Ray, rec::HitRecord, attenuation::Color, scattered::Ray) = false
