abstract type Hittable end

Base.@kwdef mutable struct HitRecord
    p::Point3 = [0.0, 0.0, 0.0]
    normal::Vec3 = [0.0, 0.0, 0.0]
    t::Float64 = 0.0
    front_face::Bool = false
end

hit!(h::Hittable, r::Ray, t_min::Float64, t_max::Float64, rec::HitRecord) = false

function set_face_normal!(rec::HitRecord, r::Ray, outward_normal::Vec3)
    rec.front_face = (r.dir⋅outward_normal) < 0
    rec.normal = rec.front_face ? outward_normal : -outward_normal
end

function copy!(h1::HitRecord, h2::HitRecord)
    h1.p = h2.p
    h1.normal = h2.normal
    h1.t = h2.t
    h1.front_face = h2.front_face
end

