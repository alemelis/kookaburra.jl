struct Sphere <: Hittable
    center::Point3
    radius::Float64
end

function hit!(sphere::Sphere, r::Ray, t_min::Float64, t_max::Float64, rec::HitRecord)
    oc = r.orig - sphere.center
    a = length_squared(r.dir)
    half_b = oc⋅r.dir
    c = length_squared(oc) - sphere.radius*sphere.radius
    Δ = half_b*half_b - a*c

    Δ<0 && (return false)

    sqrtd = √Δ

    # Find the nearest root that lies in the acceptable range
    root = (-half_b - sqrtd)/a
    if root < t_min || t_max < root
        root = (-half_b + sqrtd)/a
        if root < t_min || t_max < root
            return false
        end
    end

    rec.t = root
    rec.p = at(r, rec.t)
    outward_normal = (rec.p - sphere.center)/sphere.radius
    set_face_normal!(rec, r, outward_normal)

    true
end
