Vec3 = Vector{Float64}
Point3 = Vec3
Color = Vec3

length_squared(v::Vec3) = mapreduce(e->e*e, +, v)
magnitude(v::Vec3) = √length_squared(v)

unit_vector(v::Vec3) = v./magnitude(v)

random() = rand(3)
random(vmin::Float64, vmax::Float64) = [random_double(vmin, vmax),
                                        random_double(vmin, vmax),
                                        random_double(vmin, vmax)]

function random_in_unit_sphere()
    while true
        p = random(-1.0, 1.0)
        length_squared(p) < 1.0 && (return p)
    end
end

random_unit_vector() = unit_vector(random_in_unit_sphere())

function random_in_hemisphere(normal::Vec3)
    in_unit_sphere = random_in_unit_sphere()
    return in_unit_sphere⋅normal > 0.0 ? in_unit_sphere : -in_unit_sphere
end
