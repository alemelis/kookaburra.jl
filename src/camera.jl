struct Camera
    origin::Point3
    lower_left_corner::Point3
    horizontal::Vec3
    vertical::Vec3

end

function Camera()
    origin = [0.0, 0.0, 0.0]
    viewport_height = 2.0
    viewport_width = ASPECT_RATIO*viewport_height
    focal_length = 1.0
    horizontal = [viewport_width, 0.0, 0.0]
    vertical = [0.0, viewport_height, 0.0]
    lower_left_corner = origin - horizontal/2.0 - vertical/2.0 - [0.0, 0.0, focal_length]

    Camera(origin, lower_left_corner,
           horizontal, vertical)
end

get_ray(c::Camera, u::Float64, v::Float64) = Ray(c.origin, c.lower_left_corner +
                                                 u*c.horizontal + v*c.vertical -
                                                 c.origin)
